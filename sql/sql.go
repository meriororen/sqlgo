package sql

import (
	"fmt"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq" // for postgres driver
	gormmysql "gorm.io/driver/mysql"
	"gorm.io/driver/postgres"

	"gorm.io/gorm"
)

// ConnectReq ...
type ConnectReq struct {
	SQL     string
	Host    string
	Port    string
	DBName  string
	Pass    string
	User    string
	Options Options
}

// Options ...
type Options interface {
}

// Connector ...
type Connector struct {
	db *sqlx.DB
}

// NewSqlxConnector create new connection using sqlx
func NewSqlxConnector(crq ConnectReq) (*sqlx.DB, error) {
	datasrc := ""

	switch crq.SQL {
	case "postgres":
		datasrc = fmt.Sprintf("host=%v port=%v user=%v password=%v "+
			"dbname=%v sslmode=disable", crq.Host, crq.Port, crq.User,
			crq.Pass, crq.DBName)
	case "mysql":
		datasrc = fmt.Sprintf("%v:%v@tcp(%v:%v)/%v?parseTime=true",
			crq.User, crq.Pass, crq.Host, crq.Port, crq.DBName)
	default:
		return nil, fmt.Errorf("Unknown sql type %v", crq.SQL)
	}

	fmt.Println("Connecting sqlx with data source = ", datasrc)
	db, err := sqlx.Connect(crq.SQL, datasrc)

	return db, err
}

// NewGormConnector creates new connection to db using gorm library
func NewGormConnector(crq ConnectReq) (*gorm.DB, error) {
	var driver gorm.Dialector
	datasrc := ""

	switch crq.SQL {
	case "mysql":
		datasrc = fmt.Sprintf("%v:%v@tcp(%v:%v)/%v?parseTime=true",
			crq.User, crq.Pass, crq.Host, crq.Port, crq.DBName)
		driver = gormmysql.Open(datasrc)
	case "postgres":
		datasrc = fmt.Sprintf("host=%v port=%v user=%v password=%v "+
			"dbname=%v sslmode=disable", crq.Host, crq.Port, crq.User,
			crq.Pass, crq.DBName)
		driver = postgres.Open(datasrc)
	default:
		return nil, fmt.Errorf("Unknown sql type %v", crq.SQL)
	}

	fmt.Println("Connecting gorm with data source = ", datasrc)
	db, err := gorm.Open(driver, []gorm.Option{}...)

	return db, err
}
