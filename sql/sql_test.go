package sql_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/meriororen/sqlgo/sql"
)

func TestConnect(t *testing.T) {
	// Connector will connect to specified sql connection
	tests := []struct {
		name    string
		sql     string
		host    string
		port    string
		dbname  string
		pass    string
		user    string
		driver  string
		options sql.Options
	}{
		{
			name:   "Test connect to postgres with sqlx",
			driver: "sqlx",
			sql:    "postgres",
			host:   "127.0.0.1",
			port:   "5432",
			dbname: "test",
			pass:   "test",
			user:   "test",
		},
		{
			name:   "Test connect to mysql with gorm",
			driver: "gorm",
			sql:    "mysql",
			host:   "127.0.0.1",
			port:   "3306",
			dbname: "mysql",
			pass:   "test",
			user:   "root",
		},
		{
			name:   "Test connect to mysql with sqlx",
			driver: "sqlx",
			sql:    "mysql",
			host:   "127.0.0.1",
			port:   "3306",
			dbname: "mysql",
			pass:   "test",
			user:   "root",
		},
		{
			name:   "Test connect to postgres with gorm",
			driver: "gorm",
			sql:    "postgres",
			host:   "127.0.0.1",
			port:   "5432",
			dbname: "test",
			pass:   "test",
			user:   "test",
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			creq := sql.ConnectReq{
				SQL:     tt.sql,
				Host:    tt.host,
				Port:    tt.port,
				DBName:  tt.dbname,
				Pass:    tt.pass,
				User:    tt.user,
				Options: tt.options,
			}

			switch tt.driver {
			case "sqlx":
				_, err := sql.NewSqlxConnector(creq)
				assert.NoError(t, err)
			case "gorm":
				_, err := sql.NewGormConnector(creq)
				assert.NoError(t, err)
			}
		})
	}
}
